import Router from 'koa-router'

const router = new Router()
router.get('/api/greet', async(ctx, next) => {
    ctx.body = {msg: 'Hello World'}
})

export default router